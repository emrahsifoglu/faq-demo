import React, { Component } from 'react';

export default class CategoryItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div key={this.props.children.slug} className="col-md-12">
                <h1>{this.props.children.headline}</h1>
                <p dangerouslySetInnerHTML={{ __html: this.props.children.summary }} />
                <div>
                    <span className="badge">{this.props.children.createdAt}</span>
                    <div className="pull-left"/>
                </div>
                <hr/>
            </div>
        )
    }
}
