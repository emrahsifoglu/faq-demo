import React, { Component } from 'react';
import CategoryItem from './CategoryItem';

export default class CategoryListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let categories = this.props.items.map((category, index) => {
            return (
                <CategoryItem key={index}>{category}</CategoryItem>
            )
        });
        return (
            <div>{categories}</div>
        )
    }
}
