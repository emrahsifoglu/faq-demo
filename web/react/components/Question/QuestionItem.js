import React, { Component } from 'react';

export default class QuestionItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div key={this.props.children.slug} className="col-md-12">
                <div>
                    <span className="badge pull-right">{ this.props.children.publishAt }</span>
                    <div className="pull-left">
                        { this.props.children.headline }
                    </div>
                </div>
                <hr/>
            </div>
        )
    }
}
