import React, { Component } from 'react';
import QuestionItem from './QuestionItem';

export default class QuestionListItem extends Component {

    constructor(props) {
        super(props);
    }

    render(){
        let categories = this.props.items.map((question, index) => {
            return (
                <QuestionItem key={index}>{question}</QuestionItem>
            )
        });
        return (
            <div>{categories}</div>
        )
    }
}
