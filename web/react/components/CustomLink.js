import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class CustomLink extends Component {
    render() {
        var isActive = this.context.router.route.location.pathname === this.props.to;
        var className = isActive ? 'active' : '';

        return(
            <li className={className}><Link {...this.props}>{this.props.children}</Link></li>
        );
    }
}

CustomLink.contextTypes = {
    router: PropTypes.object
};
