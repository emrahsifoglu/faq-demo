import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Store from './stores/Store';
import Actions from './actions/Actions';
import QuestionListItem from './components/Question/QuestionListItem';

export default class QuestionView extends Component {

    constructor(props) {
        super(props);
        this.state = { questionList: Store.getQuestionList() };
    }

    componentWillMount() {
        Actions.fetchAll('front_react_question');
    }

    componentDidMount() {
        Store.addChangeListener(this.onChange.bind(this));
    }

    componentWillUnmount() {
        Store.removeChangeListener(this.onChange.bind(this));
    }

    onChange(){
        if (this.refs.view) {
            this.setState({
                questionList: Store.getQuestionList()
            })
        }
    }

    render() {
        return (
            <QuestionListItem ref="view" items={this.state.questionList}/>
        );
    }
}

