export default class Question {

    constructor(slug, headline, body, publishAt) {
        this.slug = slug;
        this.headline = headline;
        this.body = body;
        this.publishAt = publishAt;
    }

    getSlug() {
        return this.slug;
    }

    getHeadline() {
        return this.headline;
    }

    getBody() {
        return this.body;
    }

    getPublishAt() {
        return this.publishAt;
    }
}
