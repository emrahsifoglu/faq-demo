import Question from './Question';

let list = [];

export default class QuestionList {

    constructor(categories) {
        list = categories.map((question) => {
            return new Question(
                question.slug,
                question.headline,
                question.body,
                question.publishAt,
            );
        });
    }

    toArray() {
        return list;
    }

}
