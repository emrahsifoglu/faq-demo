export default class Category {

    constructor(slug, headline, body, summary, createdAt) {
        this.slug = slug;
        this.headline = headline;
        this.body = body;
        this.summary = summary;
        this.createdAt = createdAt;
    }

    getSlug() {
        return this.slug;
    }

    getHeadline() {
        return this.headline;
    }

    getBody() {
        return this.body;
    }

    getSummary() {
        return this.summary;
    }

    getCreatedAt() {
        return this.createdAt;
    }
}
