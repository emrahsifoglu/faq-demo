import Category from './Category';

let list = [];

export default class CategoryList {

    constructor(categories) {
        list = categories.map((category) => {
            return new Category(
                category.slug,
                category.headline,
                category.body,
                category.summary,
                category.createdAt,
            );
        });
    }

    toArray() {
        return list;
    }

}
