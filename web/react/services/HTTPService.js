import Fetch from 'whatwg-fetch';

class WhatWgFetch {

    constructor() {

    }

    checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        }
       throw new Error(response.statusText);
    }

    parseJSON(response) {
        return response.json()
    }

    prepareFetch(url, method, headers, body) {
        return fetch(url, {
            method: method,
            headers: headers,
            body: JSON.stringify(body)
        }).then(this.checkStatus).then(this.parseJSON);
    }

    get(path, id = '') {
        var url = path + '/' + id;
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        return this.prepareFetch(url, 'GET', headers);
    }

    post(path, data) {
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        return this.prepareFetch(path, 'POST', headers, data);
    }

    put(path, data) {
        var headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        return this.prepareFetch(path + '/' + data.id, 'PUT', headers, data.text);
    }

    destroy(path, id) {
        var headers = {
            'Accept': 'application/json'
        };
        return this.prepareFetch(path + '/' + id, 'DELETE', headers);
    }
}

const httpService = new WhatWgFetch();

export default httpService;
