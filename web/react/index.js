import React from 'react'
import ReactDOM from 'react-dom'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import CategoryView from './CategoryView'
import QuestionView from './QuestionView'
import CustomLink from './components/CustomLink';

const Header = () => (
    <nav className="navbar navbar-default">
        <div className="container-fluid">
            <div className="navbar-header">
                <Link className="navbar-brand" to="/react/">faq-demo-reactjs</Link>
            </div>
            <ul className="nav navbar-nav navbar">
                <CustomLink to="/react/category/">category</CustomLink>
                <CustomLink to="/react/question/">Questions</CustomLink>
                <li><a href="/">faq-demo-twig</a></li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
            </ul>
        </div>
    </nav>
);

const Main = () => (
    <div className="body">
        <div className="container">
            <Route exact path="/react/"/>
            <Route path="/react/category/" component={CategoryView}/>
            <Route path="/react/question/" component={QuestionView}/>
        </div>
    </div>
);

const App = () => (
    <Router>
        <div>
            <Header />
            <Main />
        </div>
    </Router>
)

ReactDOM.render((<App />), document.getElementById('app'));
