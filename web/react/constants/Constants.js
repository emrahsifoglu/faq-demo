import keyMirror from 'keymirror';

const Constants = keyMirror({
    FETCH   : null,
    FETCH_ALL   : null,
});

export default Constants;
