import { EventEmitter } from 'events';
import Fetch from 'whatwg-fetch';
import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/Constants';
import HTTPService from '../services/HTTPService';
import CategoryList from '../classes/Category/CategoryList';
import Category from '../classes/Category/Category';
import QuestionList from '../classes/Question/QuestionList';
import Question from '../classes/Question/Question';

const CHANGE_EVENT = 'change';

let list = [];
let item;

const routes = {
    'front_react_question': {
        'path': '/react/question/',
        'list': QuestionList,
        'item': Question
    },
    'front_react_category': {
        'path': '/react/category/',
        'list': CategoryList,
        'item': Category
    }
};

function parsingFailed(ex) {
    throw new Error('Parsing failed because; ' + ex);
}

function fetchItem(route, id) {
    console.log('fetchItem');
    console.log(routes[route]['path']);
    HTTPService.get(routes[route]['path'], id).then(function(data) {
        item = new routes[route]['item'](data);
        store.emitChange();
    }).catch(parsingFailed);
}

function fetchAllItem(route) {
    console.log('fetchItem(s)');
    console.log(routes[route]['path']);
    HTTPService.get(routes[route]['path']).then(function(data) {
        list = new routes[route]['list'](data).toArray();
        store.emitChange();
    }).catch(parsingFailed);
}
class Store extends EventEmitter {

    constructor() {
        super();
        AppDispatcher.register(this.handler.bind(this));
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    getCategoryList(){
        return list;
    }

    getQuestionList(){
        return list;
    }

    /**
     * @param  {Object} action
     */
    handler(action) {
        switch (action.actionType) {
            case Constants.FETCH_ALL:
                fetchAllItem(action.route);
                break;
            case Constants.FETCH:
                fetchItem(action.route, action.id);
                break;
        }
    }
}

const store = new Store();

export default store;
