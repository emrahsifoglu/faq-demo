import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/Constants';

export default  {

    fetch: (route, id) => {
        AppDispatcher.dispatch({
            actionType: Constants.FETCH,
            id: id,
            route: route
        });
    },

    fetchAll: (route) => {
        AppDispatcher.dispatch({
            actionType: Constants.FETCH_ALL,
            route: route
        });
    },

}

