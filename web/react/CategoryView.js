import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Store from './stores/Store';
import Actions from './actions/Actions';
import CategoryListItem from './components/Category/CategoryListItem';

export default class CategoryView extends Component {

    constructor(props) {
        super(props);
        this.state = { categoryList: Store.getCategoryList() };
    }

    componentWillMount() {
        Actions.fetchAll('front_react_category');
    }

    componentDidMount() {
        Store.addChangeListener(this.onChange.bind(this));
    }

    componentWillUnmount() {
        Store.removeChangeListener(this.onChange.bind(this));
    }

    onChange(){
        if (this.refs.view) {
            this.setState({
                categoryList: Store.getCategoryList()
            })
        }
    }

    render() {
        return (
            <CategoryListItem ref="view" items={this.state.categoryList}/>
        );
    }
}

