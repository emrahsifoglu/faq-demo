$(document).ready(function () {

    var questionCategory = $("#question_type_category");

    function formatRepo (repo) {
        if (repo.loading) return repo.name;

        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.name + "</div>";
        markup += "</div>";

        return markup;
    }

    function formatRepoSelection (repo) {
        $('#question_category option').each(function() {
            if ($(this).val() == repo.id) {
                $(this).text(repo.name);
            }
        });
        return repo.name || repo.text;
    }

    if (questionCategory.length > 0) {
        questionCategory.select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            placeholder: function(){
                $(this).data('Category');
            },
            ajax: {
                url: url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        name: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.totalCount
                        }
                    };
                },
                cache: false
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    }

});
