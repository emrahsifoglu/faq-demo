$(document).ready(function () {

    var url = window.location.pathname;
    var activePage = stripTrailingSlash(url);
    var section = $('#section').val();

    tinymce.PluginManager.add("advlist",function(e){function t(t){return e.$.contains(e.getBody(),t)}function n(e){return e&&/^(OL|UL|DL)$/.test(e.nodeName)&&t(e)}function r(e,t){var n=[];return t&&tinymce.each(t.split(/[ ,]/),function(e){n.push({text:e.replace(/\-/g," ").replace(/\b\w/g,function(e){return e.toUpperCase()}),data:"default"==e?"":e})}),n}function i(t,n){e.undoManager.transact(function(){var r,i=e.dom,o=e.selection;if(r=i.getParent(o.getNode(),"ol,ul"),!r||r.nodeName!=t||n===!1){var a={"list-style-type":n?n:""};e.execCommand("UL"==t?"InsertUnorderedList":"InsertOrderedList",!1,a)}r=i.getParent(o.getNode(),"ol,ul"),r&&tinymce.util.Tools.each(i.select("ol,ul",r).concat([r]),function(e){e.nodeName!==t&&n!==!1&&(e=i.rename(e,t)),i.setStyle(e,"listStyleType",n?n:null),e.removeAttribute("data-mce-style")}),e.focus()})}function o(t){var n=e.dom.getStyle(e.dom.getParent(e.selection.getNode(),"ol,ul"),"listStyleType")||"";t.control.items().each(function(e){e.active(e.settings.data===n)})}var a,s,l=function(e,t){var n=e.settings.plugins?e.settings.plugins:"";return tinymce.util.Tools.inArray(n.split(/[ ,]/),t)!==-1};a=r("OL",e.getParam("advlist_number_styles","default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman")),s=r("UL",e.getParam("advlist_bullet_styles","default,circle,disc,square"));var u=function(t){return function(){var r=this;e.on("NodeChange",function(e){var i=tinymce.util.Tools.grep(e.parents,n);r.active(i.length>0&&i[0].nodeName===t)})}};l(e,"lists")&&(e.addCommand("ApplyUnorderedListStyle",function(e,t){i("UL",t["list-style-type"])}),e.addCommand("ApplyOrderedListStyle",function(e,t){i("OL",t["list-style-type"])}),e.addButton("numlist",{type:a.length>0?"splitbutton":"button",tooltip:"Numbered list",menu:a,onPostRender:u("OL"),onshow:o,onselect:function(e){i("OL",e.control.settings.data)},onclick:function(){i("OL",!1)}}),e.addButton("bullist",{type:s.length>0?"splitbutton":"button",tooltip:"Bullet list",onPostRender:u("UL"),menu:s,onshow:o,onselect:function(e){i("UL",e.control.settings.data)},onclick:function(){i("UL",!1)}}))});
    tinymce.PluginManager.load('colorpicker', '/vendor-dp/tinymce/plugins/colorpicker/plugin.min.js');
    tinymce.PluginManager.load('textcolor', '/vendor-dp/tinymce/plugins/textcolor/plugin.min.js');
    tinymce.PluginManager.load('code', '/vendor-dp/tinymce/plugins/code/plugin.min.js');
    tinymce.PluginManager.load('link', '/vendor-dp/tinymce/plugins/link/plugin.min.js');
    tinymce.PluginManager.load('preview', '/vendor-dp/tinymce/plugins/preview/plugin.min.js');
    tinymce.PluginManager.load('lists', '/vendor-dp/tinymce/plugins/lists/plugin.min.js');
    tinymce.PluginManager.load('advlist', '/vendor-dp/tinymce/plugins/advlist/plugin.min.js');
    tinymce.PluginManager.load('image', '/vendor-dp/tinymce/plugins/image/plugin.min.js');
    tinymce.PluginManager.load('imagetools', '/vendor-dp/tinymce/plugins/imagetools/plugin.min.js');
    tinymce.PluginManager.load('autoresize', '/vendor-dp/tinymce/plugins/autoresize/plugin.min.js');

    tinymce.init({
        selector: '.tinymce',
        theme_url: '/vendor-dp/tinymce/theme.js',
        skin_url: '/vendor-dp/tinymce',
        plugins: "code link textcolor colorpicker preview advlist lists advlist image imagetools autoresize",
        toolbar: "code link bullist numlist forecolor backcolor  image imagetools fontsizeselect fontselect",
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        font_formats: 'Poetsen one regular=poetsen_oneregular;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
        content_css : "/css/fonts.css",
        image_title: true,
        automatic_uploads: true,
        images_upload_url: '/vendor-dp/tinymce/postAcceptor.php',
        file_picker_types: 'image',
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            // Note: In modern browsers input[type="file"] is functional without
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var blobInfo = blobCache.create(id, file, reader.result);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                };
            };

            input.click();
        }
    });

    $('.date-time-picker').datetimepicker({
        viewMode: 'months',
        format: 'DD/MM/YYYY HH:mm:ss'
    });

    $(".date-picker").datetimepicker({
        viewMode: 'months',
        format: 'DD/MM/YYYY'
    });

    $('.nav li a').each(function(){
        var currentPage = stripTrailingSlash($(this).attr('href'));
        if (activePage.indexOf(currentPage) == 0 || $(this).data()['section'] == section) {
            $(this).parent().addClass('active');
            return false;
        }
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

});
