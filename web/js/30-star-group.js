$(document).ready(function () {

    var stars = $('div.star.enabled');

    stars.click(function () {
        var contentId = $(this).closest('.star-group').data()['id'];
        var starNum = $(this).parent().data()['starNum'];
        var rate = $(this).data()['rate'];
        for (var i = 1; i <= starNum; i++ ) {
            var star = $('#starGroup' + contentId + ' .star-bar #star' + i);
            star.unbind('click');
            star.unbind('mouseover');
            star.unbind('mouseout');
            star.removeClass('star-hover');
        }
        $.ajax({
            type: 'POST',
            url: $('#contentRateUrl').val(),
            data:  { contentId: contentId, rate: rate, starNum: starNum },
            dataType: 'json',
            timeout: 3000,
            success: function(response) {
                var starGroup = $('#starGroup' + contentId);
                starGroup.html(response.starGroup);
            },
            error: function() {
                console.log('rating problem');
            }
        });
    });

    stars.mouseover(function () {
        var contentId = $(this).closest('.star-group').data()['id'];
        var starNum = $(this).parent().data()['starNum'];
        var rate = $(this).data()['rate'];
        for (var i = 1; i <= starNum; i++ ) {
            $('#starGroup' + contentId + ' .star-bar #star' + i).toggleClass('star-hover', i <= rate);
        }
    });

    stars.mouseout(function () {
        var contentId = $(this).closest('.star-group').data()['id'];
        var starNum = $(this).parent().data()['starNum'];
        var rate = $(this).data()['rate'];
        for (var i = 1; i <= starNum; i++ ) {
            $('#starGroup' + contentId + ' .star-bar #star' + i).removeClass('star-hover');
        }
    });

});
