<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170702225945 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, content_rate_id INT DEFAULT NULL, headline VARCHAR(255) NOT NULL, body LONGTEXT DEFAULT NULL, is_active TINYINT(1) NOT NULL, publish_at DATETIME NOT NULL, expires_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, slug VARCHAR(100) NOT NULL, INDEX IDX_B6F7494E12469DE2 (category_id), UNIQUE INDEX UNIQ_B6F7494E6391462C (content_rate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(40) NOT NULL, role VARCHAR(50) NOT NULL, password VARCHAR(64) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_rate (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, rate INT NOT NULL, content_id INT NOT NULL, content_type ENUM(\'CATEGORY\', \'QUESTION\') NOT NULL COMMENT \'(DC2Type:ContentType)\', created_at DATETIME NOT NULL, INDEX IDX_A56D73F0A76ED395 (user_id), INDEX user_content_idx (user_id, content_id), UNIQUE INDEX user_content_id (user_id, content_id, content_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, content_rate_id INT DEFAULT NULL, headline VARCHAR(255) NOT NULL, body LONGTEXT DEFAULT NULL, summary LONGTEXT DEFAULT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, slug VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_64C19C16391462C (content_rate_id), INDEX is_active_idx (is_active), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content_rate (id INT AUTO_INCREMENT NOT NULL, rate INT NOT NULL, rank INT NOT NULL, content_type ENUM(\'CATEGORY\', \'QUESTION\') NOT NULL COMMENT \'(DC2Type:ContentType)\', created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E6391462C FOREIGN KEY (content_rate_id) REFERENCES content_rate (id)');
        $this->addSql('ALTER TABLE user_rate ADD CONSTRAINT FK_A56D73F0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C16391462C FOREIGN KEY (content_rate_id) REFERENCES content_rate (id)');
    }

    public function postUp(Schema $schema) {
        $this->connection->executeQuery('INSERT INTO `user` (`id`, `email`, `name`, `role`, `password`, `created_at`, `updated_at`) VALUES (1, \'admin@example.com\', \'admin\', \'ROLE_ADMIN\', \'$2y$13$sunBXcB.asGKFtgsorBnBuQzFFjtul4olkgIv0szUC5qKQOGeA4K6\', \'2017-06-29 21:00:00\', NULL);');
    }


    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_rate DROP FOREIGN KEY FK_A56D73F0A76ED395');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E12469DE2');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E6391462C');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C16391462C');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_rate');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE content_rate');
    }
}
