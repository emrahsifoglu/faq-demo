# FAQ demo

## Getting Started

### Technologies used

* [PhpStorm](https://www.jetbrains.com/phpstorm/) - The IDE
* [Symfony](https://symfony.com/doc/2.8/setup.html) - The Framework

Symfony Components

* [DoctrineMigrationsBundle](http://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html)
* [DoctrineEnumBundle](https://github.com/fre5h/DoctrineEnumBundle)
* [LexikFormFilterBundle](https://github.com/lexik/LexikFormFilterBundle)
* [AsseticBundle](https://github.com/symfony/assetic-bundle)
* [JMSTranslationBundle](https://github.com/schmittjoh/JMSTranslationBundle)
* [JMSDiExtraBundle](https://github.com/schmittjoh/JMSDiExtraBundle)
* [DoctrineExtensions](https://github.com/Atlantic18/DoctrineExtensions/blob/master/doc/symfony2.md)

JS-Tools

* [jQuery](https://jquery.com/)
* [Select2](https://select2.github.io/)
* [Bootstrap 3 Datepicker](https://eonasdan.github.io/bootstrap-datetimepicker/)
* [TinyMCE](https://www.tinymce.com/)

### Prerequisites

* Composer 
* Apache

You may use Nginx or another environment to run php.

### Installing

Install PHP packages
```
composer install
```
Clear cache
```
php app/console cache:clear

```
Execute migration(s)
```
php app/console doctrine:migrations:migrate
```
Install and dump assets
```
php app/console assets:install
php app/console assetic:dump
```
## Running

Run Symfony application
```
php app/console server:start
```
### How it works?

After successfully set up the page, you can use following credentials to login as admin.

- Email: admin@example.com
- Password: admin

With this privilege, you will be allowed to access to administration, where you can perform crud operations on users, categories and questions.

As a user, you can only view contents but after logging, you can use voting feature on questions.

Site is mobile friendly also supports multi languages. 

English is set as default. Admin can change translations via web interface.

### Cons

- No tests written.
- Dry can be performed better.
- User interface can be improved.
- Some other features such as sorting, pagination can also be added.
- User may have more than one role.

## Testing

There are no tests to run

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is a task thus source is kept in a private repo.

Resources
========
- http://symfony.com/doc/current/setup.html
- https://github.com/pedroresende/reactjs-symfony-boiler-plate
- https://github.com/hanernlee/faq-app-react
- https://github.com/FLM/symfony-webpack-react
- https://github.com/genj/GenjFaqBundle
- https://github.com/genj/faq-demo
- http://www.tipocode.com/symfony2/web/starratingsystem
- http://www.tipocode.com/jquery/very-precise-jquery-ajax-star-rating-plugin-tutorial/
- https://stackoverflow.com/questions/20956660/how-to-render-twig-output-to-a-variable-for-later-use-symfony2
- https://stackoverflow.com/questions/15707484/how-to-get-request-object-inside-a-twig-extension-in-symfony
- https://codepen.io/pshrmn/pen/YZXZqM?editors=0010
- https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf
- https://stackoverflow.com/questions/34418254/is-there-a-way-using-react-router-to-set-an-active-class-on-the-wrapper-to-the-l
- https://github.com/insin/react-router-active-component