<?php

namespace AppBundle\Entity\Question;

use Doctrine\ORM\EntityManager;

class QuestionFacade
{

    protected $entityManager;

    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function getRepository() {
        return $this->entityManager->getRepository('AppBundle:Question\Question');
    }

    public function save(Question $question, $andFlush = true) {
        $this->entityManager->persist($question);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function delete(Question $question, $andFlush = true) {
        $this->entityManager->remove($question);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function getAllQuestions() {
        return $this->getRepository()->findAll();
    }

    public function getPublicQuestions() {
        return $this->getRepository()->findAllPublic();
    }

    /**
     * @return QuestionList
     */
    public function getQuestionList() {
        return new QuestionList($this->getPublicQuestions());
    }


}
