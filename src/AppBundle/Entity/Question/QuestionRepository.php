<?php

namespace AppBundle\Entity\Question;

use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{

    public function findAll() {
        return $this->createQueryBuilder('q')
            ->select('q')
            ->addSelect('c')
            ->addSelect('c_r')
            ->leftJoin('q.category', 'c')
            ->leftJoin('q.contentRate', 'c_r')
            ->getQuery()
            ->getResult();
    }

    public function findTotalActiveCount() {
        return $this->createQueryBuilder('q')
            ->select('COUNT(q)')
            ->where('q.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllByPartialQuery($searchQuery, $limit = null) {
        $query = $this->createQueryBuilder('q')
            ->where('q.headline like :searchQuery or q.body like :searchQuery')
            ->setParameter('searchQuery', '%'. $searchQuery . '%');

        if ($limit) {
            $query->setMaxResults(10);
        }

        return $query->getQuery();
    }

    /**
     * @param string $categorySlug
     * @return Question|null
     */
    public function findOneFirstByCategorySlug($categorySlug) {
        return $this->createQueryBuilder('q')
            ->join('q.category', 'c')
            ->where('c.slug = :categorySlug')
            ->andWhere('q.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('categorySlug', $categorySlug)
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int $max
     * @return array
     */
    public function findOneMostRecent($max) {
         return $this->createQueryBuilder('q')
             ->join('q.category', 'c')
             ->where('q.isActive = :isActive')
             ->andWhere('q.publishAt <= :publishAt')
             ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
             ->setParameter('isActive', true)
             ->setParameter('publishAt', date('Y-m-d H:i:s'))
             ->setParameter('expiresAt', date('Y-m-d H:i:s'))
             ->orderBy('q.publishAt', 'DESC')
             ->setMaxResults($max)
             ->getQuery()
             ->getResult();
    }

    /**
     * @return array
     */
    public function findAllPublic() {
        return $this->createQueryBuilder('q')
            ->join('q.category', 'c')
            ->leftJoin('q.contentRate', 'content_rate')
            ->where('q.isActive = :isActive')
            ->andWhere('c.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->orderBy('q.publishAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $slug
     *
     * @return null|Question
     */
    public function findOnePublicBySlug($slug) {
        return $this->createQueryBuilder('q')
            ->join('q.category', 'c')
            ->leftJoin('q.contentRate', 'content_rate')
            ->where('q.slug = :slug')
            ->andWhere('q.isActive = :isActive')
            ->andWhere('c.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('slug', $slug)
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @return null|Question
     */
    public function findOnePublicById($id) {
        return $this->createQueryBuilder('q')
            ->join('q.category', 'c')
            ->leftJoin('q.contentRate', 'content_rate')
            ->where('q.id = :id')
            ->andWhere('q.isActive = :isActive')
            ->andWhere('c.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('id', $id)
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $searchQuery
     * @param int $max
     * @return array
     */
    public function findAllPublicByQuery($searchQuery, $max) {
        return $this->createQueryBuilder('q')
            ->join('q.category', 'c')
            ->leftJoin('q.contentRate', 'content_rate')
            ->where('q.headline like :searchQuery or q.body like :searchQuery')
            ->andWhere('q.isActive = :isActive')
            ->andWhere('c.isActive = :isActive')
            ->andWhere('q.publishAt <= :publishAt')
            ->andWhere('(q.expiresAt IS NULL OR q.expiresAt >= :expiresAt)')
            ->setParameter('searchQuery', '%'. $searchQuery . '%')
            ->setParameter('isActive', true)
            ->setParameter('publishAt', date('Y-m-d H:i:s'))
            ->setParameter('expiresAt', date('Y-m-d H:i:s'))
            ->orderBy('q.publishAt', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

}
