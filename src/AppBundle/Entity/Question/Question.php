<?php

namespace AppBundle\Entity\Question;

use AppBundle\Entity\Category\Category;
use AppBundle\Entity\ContentRate\ContentRate;
use AppBundle\Entity\ContentRate\IContentRate;
use AppBundle\Entity\IEntity;
use DateTime;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="QuestionRepository")
 */
class Question implements IEntity, IContentRate
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category\Category", inversedBy="questions")
     */
    protected $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $headline;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $body;

    /**
     * @var ContentRate
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContentRate\ContentRate", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="content_rate_id", referencedColumnName="id")
     */
    protected $contentRate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $publishAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @Gedmo\Slug(fields={"headline"}, updatable=false)
     * @ORM\Column(type="string", length=100)
     */
    protected $slug;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set headline
     *
     * @param string $headline
     *
     * @return Question
     */
    public function setHeadline($headline) {
        $this->headline = $headline;

        return $this;
    }

    /**
     * Get headline
     *
     * @return string
     */
    public function getHeadline() {
        return $this->headline;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Question
     */
    public function setBody($body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * @return ContentRate
     */
    public function getContentRate() {
        return $this->contentRate;
    }

    /**
     * @param ContentRate $contentRate
     */
    public function setContentRate(ContentRate $contentRate) {
        $this->contentRate = $contentRate;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Question
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return string
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * Set publishAt
     *
     * @param DateTime $publishAt
     *
     * @return Question
     */
    public function setPublishAt($publishAt) {
        $this->publishAt = $publishAt;

        return $this;
    }

    /**
     * Get publishAt
     *
     * @return DateTime
     */
    public function getPublishAt() {
        return $this->publishAt;
    }

    /**
     * Set expiresAt
     *
     * @param DateTime $expiresAt
     *
     * @return Question
     */
    public function setExpiresAt($expiresAt) {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return DateTime
     */
    public function getExpiresAt() {
        return $this->expiresAt;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Question
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Question
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Question
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Question
     */
    public function setCategory(Category $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory() {
        return $this->category;
    }

    public function getRate() {
        return ($this->contentRate) ? $this->contentRate->getRate() : 0;
    }

    public function getRank() {
        return ($this->contentRate) ? $this->contentRate->getRank() : 0;
    }

    /**
     * Returns a string representation of this object
     *
     * @return string
     */
    public function __toString() {
        return (string) $this->getHeadline();
    }

    /**
     * Is visible for user?
     *
     * @return boolean
     */
    public function isPublic() {
        if ($this->getIsActive() && ($this->getPublishAt()->getTimestamp() < time()) && (!$this->getExpiresAt() || $this->getExpiresAt()->getTimestamp() > time())) {
            return true;
        }

        return false;
    }

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy() {
        return [
            'slug' => $this->slug,
            'headline' => $this->headline,
            'body' => $this->body,
            'publishAt' => $this->publishAt->format('d/m/Y'),
        ];
    }
}
