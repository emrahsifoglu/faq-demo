<?php

namespace AppBundle\Entity\User;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity(repositoryClass="UserRateRepository")
 * @ORM\Table(
 *     name="user_rate",
 *     indexes={@ORM\Index(name="user_content_idx", columns={"user_id", "content_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="user_content_id", columns={"user_id", "content_id", "content_type"})}
 * )
 */
class UserRate
{

    /**
     * @ORM\Id;
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\User", inversedBy="rates")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * */
    protected $user;

    /**
     * @var int
     *
     * @ORM\Column(name="rate", type="integer", nullable=false)
     */
    protected $rate;

    /**
     * @var int
     *
     * @ORM\Column(name="content_id", type="integer", nullable=false)
     */
    protected $contentId;

    /**
     * @ORM\Column(name="content_type", type="ContentType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ContentType")
     */
    protected $contentType;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    public function __construct(

    ) {
        $this->rate = 0;
    }

    public function getId() {
        return $this->id;
    }



    /**
     * @return int
     */
    public function getRate() {
        return $this->rate;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user) {
        $this->user = $user;
    }

    /**
     * @param int $rate
     */
    public function setRate($rate) {
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getContentId() {
        return $this->contentId;
    }

    /**
     * @param int $contentId
     */
    public function setContentId($contentId) {
        $this->contentId = $contentId;
    }

    /**
     * @return string
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

}
