<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFacade
{

	protected $entityManager;
	protected $userPasswordEncoder;

	public function __construct(
		EntityManager $entityManager,
        UserPasswordEncoderInterface $userPasswordEncoder
	) {
		$this->entityManager = $entityManager;
		$this->userPasswordEncoder = $userPasswordEncoder;
	}

    public function getRepository() {
		return $this->entityManager->getRepository('AppBundle:User\User');
	}

	public function updateUser(User $user, $andFlush = true) {
	    if ($user->getPlainPassword()) {
            $password = $this->userPasswordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
        }
        $this->save($user, $andFlush);
    }

    public function save(User $user, $andFlush = true) {
        $this->entityManager->persist($user);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function delete(User $user, $andFlush = true) {
        $this->entityManager->remove($user);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function getUsers() {
	    return $this->getRepository()->findAll();
    }

}
