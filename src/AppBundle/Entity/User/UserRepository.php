<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class UserRepository extends EntityRepository {

	/**
	 * @param array $ids
	 * @return array
	 */
	public function findAllByIds(array $ids) {
		return $this->createQueryBuilder('u')
			->where('u.id IN (:ids)')
			->setParameter('ids', $ids)
			->getQuery()
			->getResult();
	}

	/**
	 * @param $email
	 * @return null|User
	 * @throws NonUniqueResultException
	 */
	public function findOneByEmail($email) {
		return $this->createQueryBuilder('u')
			->where('u.email = :email')
			->setParameter('email', $email)
			->getQuery()
			->getOneOrNullResult();
	}

    /**
     * @return int
     */
    public function findTotalCount() {
        return $this->createQueryBuilder('u')
			->select('COUNT(u)')
            ->getQuery()
			->getSingleScalarResult();
	}



}
