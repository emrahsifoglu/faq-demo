<?php

namespace AppBundle\Entity\User;

use AppBundle\DBAL\Types\ContentType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @UniqueEntity(fields="email", message="user.email.already_used")
 */
class User implements UserInterface
{

	/**
	 * @ORM\Id;
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", length=40)
     * @Assert\Length(max=40)
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=50)
     * @Assert\Length(max=50)
	 */
	protected $role;

	/**
	 * @Assert\Length(max=4096)
	 */
	protected $plainPassword;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	protected $password;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\UserRate", mappedBy="user", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    protected $rates;

	/**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct(
        
    ) {
        $this->rates = new ArrayCollection();
    }

    public function getId() {
		return $this->id;
	}

    public function getUsername() {
		return $this->email;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role = null) {
        $this->role = $role;
    }

    public function getRoles() {
        return [$this->getRole()];
    }

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function getPlainPassword() {
		return $this->plainPassword;
	}

	public function setPlainPassword($plainPassword) {
		$this->plainPassword = $plainPassword;
	}

    public function eraseCredentials() {
        return null;
    }
	
	public function getSalt() {
		return null;
	}

    public function getRates() {
        return $this->rates;
    }

	public function hasQuestionRated($id) {
        /** @var UserRate $rate */
        foreach ($this->rates as $rate) {
            if ($rate->getContentType() == ContentType::QUESTION && $rate->getContentId() == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param UserRate $userRate
     * @return User
     */
    public function addRate(UserRate $userRate) {
        if (!$this->rates->contains($userRate)) {
            $this->rates->add($userRate);
            $userRate->setUser($this);
        }
        return $this;
    }

    /**
     * @param UserRate $userRate
     * @return User
     */
    public function removeRate(UserRate $userRate) {
        if ($this->rates->contains($userRate)) {
            $this->rates->removeElement($userRate);
        }
        return $this;
    }

}