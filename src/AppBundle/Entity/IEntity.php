<?php

namespace AppBundle\Entity;

interface IEntity
{

    public function getArrayCopy();

}
