<?php

namespace AppBundle\Entity\Category;

use AppBundle\Entity\ContentRate\ContentRate;
use AppBundle\Entity\Question\Question;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\IEntity;

/**
 * @ORM\Entity(repositoryClass="CategoryRepository")
 * @ORM\Table(
 *     name="category",
 *     indexes={@ORM\Index(name="is_active_idx", columns={"is_active"})}
 * )
 */
class Category implements IEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Question\Question", mappedBy="category")
     */
    protected $questions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $headline;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $body;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $summary;

    /**
     * @var ContentRate
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContentRate\ContentRate", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="content_rate_id", referencedColumnName="id")
     */
    protected $contentRate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @Gedmo\Slug(fields={"headline"}, updatable=false)
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;

    public function __construct() {
        $this->questions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set headline
     *
     * @param string $headline
     *
     * @return Category
     */
    public function setHeadline($headline) {
        $this->headline = $headline;

        return $this;
    }

    /**
     * Get headline
     *
     * @return string
     */
    public function getHeadline() {
        return $this->headline;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Category
     */
    public function setBody($body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary() {
        return $this->summary;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Category
     */
    public function setSummary($summary) {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return ContentRate
     */
    public function getContentRate() {
        return $this->contentRate;
    }

    public function getRate() {
        return ($this->contentRate) ? $this->contentRate->getRate() : 0;
    }

    public function getRank() {
        return ($this->contentRate) ? $this->contentRate->getRank() : 0;
    }

    /**
     * @param ContentRate $contentRate
     */
    public function setContentRate(ContentRate $contentRate) {
        $this->contentRate = $contentRate;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     *
     * @return Category
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return Category
     */
    public function setCreatedAt(DateTime $createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param DateTime $updatedAt
     *
     * @return Category
     */
    public function setUpdatedAt(DateTime $updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Add question
     *
     * @param Question $question
     *
     * @return Category
     */
    public function addQuestion(Question $question) {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
        }

        return $this;
    }

    /**
     * Remove question
     *
     * @param Question $question
     */
    public function removeQuestion(Question $question) {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
        }
    }

    /**
     * Get questions
     *
     * @return ArrayCollection
     */
    public function getQuestions() {
        return $this->questions;
    }

    public function hasQuestions() {
        return $this->questions->count() > 0;
    }

    /**
     * Returns a string representation of this object
     *
     * @return string
     */
    public function __toString() {
        return (string) $this->getHeadline();
    }

    /**
     * Get array copy of object
     *
     * @return array
     */
    public function getArrayCopy() {
        return [
          'slug' => $this->slug,
          'headline' => $this->headline,
          'body' => $this->body,
          'summary' => $this->summary,
          'createdAt' => $this->createdAt->format('d/m/Y'),
        ];
    }
}
