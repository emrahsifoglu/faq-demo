<?php

namespace AppBundle\Entity\Category;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{

    public function findAll() {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->addSelect('q')
            ->leftJoin('c.questions', 'q')
            ->leftJoin('c.contentRate', 'c_r')
            ->getQuery()
            ->getResult();
    }

    public function findAllActive() {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->addSelect('q')
            ->leftJoin('c.questions', 'q')
            ->leftJoin('c.contentRate', 'c_r')
            ->where('c.isActive = :isActive')
            ->setParameter('isActive', true)
            ->getQuery()
            ->getResult();
    }

    public function findTotalActiveCount() {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c)')
            ->where('c.isActive = :isActive')
            ->setParameter('isActive', true)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllByPartialQuery($searchQuery, $limit = null) {
        $query = $this->createQueryBuilder('c')
            ->where('c.headline like :searchQuery or c.body like :searchQuery')
            ->setParameter('searchQuery', '%'. $searchQuery . '%');

        if ($limit) {
            $query->setMaxResults(10);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function findOneActiveBySlug($slug) {
        $query = $this->createQueryBuilder('c')
            ->select('c')
            ->addSelect('q')
            ->leftJoin('c.questions', 'q')
            ->leftJoin('c.contentRate', 'c_r')
            ->where('c.isActive = :isActive')
            ->andWhere('c.slug = :slug')
            ->setParameter('isActive', true)
            ->setParameter('slug', $slug)
            ->getQuery();

        if($result = $query->getResult()) {
            return $result[0];
        }

        return null;
    }

    /**
     * @return Category|null
     */
    public function findOneFirst() {
       return $this->createQueryBuilder('c')
           ->leftJoin('c.contentRate', 'c_r')
           ->where('c.isActive = :isActive')
           ->setParameter('isActive', true)
           ->setMaxResults(1)
           ->getQuery()
           ->getOneOrNullResult();
    }

}