<?php

namespace AppBundle\Entity\Category;

use Doctrine\ORM\EntityManager;

class CategoryFacade
{

    protected $entityManager;

    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function getRepository() {
        return $this->entityManager->getRepository('AppBundle:Category\Category');
    }

    public function save(Category $category, $andFlush = true) {
        $this->entityManager->persist($category);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function delete(Category $category, $andFlush = true) {
        $this->entityManager->remove($category);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    public function getAllCategories() {
        return $this->getRepository()->findAll();
    }

    public function getActiveCategories() {
        return $this->getRepository()->findAllActive();
    }

    /**
     * @return CategoryList
     */
    public function getCategoryList() {
        return new CategoryList($this->getActiveCategories());
    }

}
