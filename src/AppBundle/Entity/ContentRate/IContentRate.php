<?php

namespace AppBundle\Entity\ContentRate;

interface IContentRate
{

    public function getRate();
    public function getRank();

}
