<?php

namespace AppBundle\Entity\ContentRate;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity(repositoryClass="ContentRateRepository")
 */
class ContentRate implements IContentRate
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="rate", type="integer", nullable=false)
     */
    protected $rate;

    /**
     * @var int
     * @ORM\Column(name="rank", type="integer", nullable=false)
     */
    protected $rank;

    /**
     * @ORM\Column(name="content_type", type="ContentType", nullable=false)
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ContentType")
     */
    protected $contentType;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    public function getId() {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRate() {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate($rate) {
        $this->rate = $rate;
    }

    /**
     * Get rank
     *
     * @return int
     */
    public function getRank() {
        return $this->rank;
    }

    /**
     * Set rank
     *
     * @param int $rank
     * @return ContentRate
     */
    public function setRank($rank) {
        $this->rank = $rank;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }

    public function getAverage() {
        return ContentRateFacade::ComputeAverage($this->rate, $this->rank);
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

}