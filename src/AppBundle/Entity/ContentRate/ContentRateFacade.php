<?php

namespace AppBundle\Entity\ContentRate;

use AppBundle\DBAL\Types\ContentType;
use AppBundle\Entity\Question\Question;
use AppBundle\Entity\User\User;
use AppBundle\Entity\User\UserFacade;
use AppBundle\Entity\User\UserRate;
use Doctrine\ORM\EntityManager;

class ContentRateFacade
{

    protected $entityManager;
    protected $userFacade;

    public function __construct(
        EntityManager $entityManager,
        UserFacade $userFacade
    ) {
        $this->entityManager = $entityManager;
        $this->userFacade = $userFacade;
    }

    public function rateQuestion(User $user, Question $question, $rate) {
        $questionRate = $this->createContentRate(ContentType::QUESTION, $rate);
        if ($question->getContentRate()) {
            $questionRate = $question->getContentRate();
            $questionRate->setRate($questionRate->getRate() + $rate);
            $questionRate->setRank($questionRate->getRank() + 1);
        }
        $question->setContentRate($questionRate);

        $userRate = $this->createUserRate($question->getId(), ContentType::QUESTION, $rate);
        $user->addRate($userRate);

        $this->entityManager->persist($userRate);
        $this->userFacade->save($user);

        return $questionRate;
    }

    public function createContentRate($type, $rate) {
        $contentRate = new ContentRate();
        $contentRate->setContentType($type);
        $contentRate->setRate($rate);
        $contentRate->setRank(1);
        return $contentRate;
    }

    public function createUserRate($contentId, $contentType, $rate) {
        $userRate = new UserRate();
        $userRate->setContentId($contentId);
        $userRate->setContentType($contentType);
        $userRate->setRate($rate);
        return $userRate;
    }

    public function prepareView($contentId, $rate, $rank, $starNum, $starWidth, $status) {
        $average = self::ComputeAverage($rate, $rank);

        $nbrPixelsInDiv = $starNum * $starWidth;
        $numEnlightedPX = round($nbrPixelsInDiv * $average / $starNum, 0);

        $style = [
            'width' => $nbrPixelsInDiv,
            'height' => $starWidth,
            'lighted' => $numEnlightedPX,
            'len' => $nbrPixelsInDiv,
        ];

        return [
            'view' => '@App/@blocks/starGroup.html.twig',
            'params' => [
                'status' => $status,
                'contentId' => $contentId,
                'average' => $average,
                'rank' => $rank,
                'starNum' => $starNum,
                'style' => $style
            ]
        ];
    }

    public static function ComputeAverage($rate, $rank)  {
        if ($rate  && $rank) {
            return round($rate / $rank, 2);
        }

        return 0;
    }
}
