<?php

namespace AppBundle\Validator\Constraints\PasswordNotMatch;

use AppBundle\Entity\User\User;
use AppBundle\Entity\User\UserFacade;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class PasswordNotMatchValidator extends ConstraintValidator {

	private $em;
	protected $userFacade;
    protected $userPasswordEncoder;

	public function __construct(
		EntityManager $em,
		UserFacade $userFacade,
        UserPasswordEncoderInterface $userPasswordEncoder
	) {
		$this->em = $em;
		$this->userFacade = $userFacade;
        $this->userPasswordEncoder = $userPasswordEncoder;
	}

	public function validate($object, Constraint $constraint) {
		/** @var User $data */
		$data = $this->context->getRoot()->getData();

		$id = $data->getId();
		if (!$id) {
			return;
		}

        if ($data->getPlainPassword()) {
            $user = $this->userFacade->getRepository()->findOneByEmail($data->getEmail());
            if (!$this->userPasswordEncoder->isPasswordValid($user, $object)) {
                $this->context->addViolation($constraint->message);
            }
        }

	}

    public function encodePassword($raw, $salt)
    {
        return hash('sha256', $salt . $raw); // Custom function for encrypt with sha256
    }

    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }

}
