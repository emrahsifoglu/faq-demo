<?php

namespace AppBundle\Validator\Constraints\PasswordNotMatch;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class PasswordNotMatch extends Constraint {

	public $message = 'Current password don\'t match';

	public function __construct($message) {
	    $this->message = $message;
        parent::__construct();
    }

    public function validatedBy() {
		return 'password_not_match_validator';
	}

}
