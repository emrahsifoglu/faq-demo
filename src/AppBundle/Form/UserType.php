<?php

namespace AppBundle\Form;

use AppBundle\Entity\User\User;
use AppBundle\Services\RolesHelper;
use AppBundle\Validator\Constraints\PasswordNotMatch\PasswordNotMatch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

class UserType extends AbstractType
{

    protected $rolesHelper;
    protected $translator;

    public function __construct(
        TranslatorInterface $translator,
        RolesHelper $rolesHelper
    ) {
        $this->translator = $translator;
        $this->rolesHelper = $rolesHelper;
    }

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('name', TextType::class, [
            'label' => 'form.label.name',
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'form.label.email'
        ]);

        $builder->add('plainPassword', RepeatedType::class, [
            'required' => $options['intention'] == 'create',
            'type' => PasswordType::class,
            'first_options' => ['label' => 'form.label.password'],
            'second_options' => ['label' => 'form.label.password_repeat'],
            'invalid_message' => 'user.plain_password.invalid'
        ]);

        if ($options['editor'] == 'admin') {
            $builder->add('role', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => $this->rolesHelper->flattenAssocArray(),
                'placeholder' => 'form.empty_value.not_set'
            ]);
        } else {
            $builder->add('currentPassword', PasswordType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new PasswordNotMatch($this->translator->trans('user.current_password.invalid', [], 'validators'))
                ]
            ]);
        }
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'data_class' => User::class,
            'translation_domain' => 'form',
            'intention' => null,
            'editor' => null,
		]);
	}

    public function getName() {
        return 'user_type';
    }

}
