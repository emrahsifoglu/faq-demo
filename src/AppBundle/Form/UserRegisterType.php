<?php

namespace AppBundle\Form;

use AppBundle\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType {

    /**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', TextType::class, [
            'label' => 'form.label.name'
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'form.label.email'
        ]);

        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'first_options' => ['label' => 'form.label.password'],
            'second_options' => ['label' => 'form.label.password_repeat'],
            'invalid_message' => 'user.plain_password.invalid'
        ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmitData']);
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmitData(FormEvent $event) {
        $form = $event->getForm();
        /** @var User $data */
        $data = $event->getData();
        if(!$data) {
            return;
        }

        $data->setRole('ROLE_USER');
    }

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'data_class' => User::class,
            'translation_domain' => 'form'
		]);
	}

	public function getName() {
		return 'user_register_type';
	}

}
