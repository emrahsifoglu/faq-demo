<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('headline', TextType::class, [
            'required' => true,
            'label' => 'form.label.headline'
        ]);
        $builder->add('body', TextareaType::class, [
            'attr' => [
                'class' => 'tinymce',
                'data-theme' => 'bbcode' // Skip it if you want to use default theme
            ],
            'required' => false,
            'label' => 'form.label.body'
        ]);
        $builder->add('summary', TextareaType::class, [
            'attr' => [
                'class' => 'tinymce',
                'data-theme' => 'bbcode' // Skip it if you want to use default theme
            ],
            'required' => false,
            'label' => 'form.label.summary'
        ]);
        $builder->add('isActive', CheckboxType::class, [
            'required' => false,
            'label' => 'form.label.isActive'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Category::class,
            'translation_domain' => 'form'
        ]);
    }

    public function getName() {
        return 'category_type';
    }

}
