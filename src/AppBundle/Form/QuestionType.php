<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category\Category;
use AppBundle\Entity\Question\Question;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{

    protected $entityManager;

    public function __construct(
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('headline', TextType::class, [
            'required' => true,
            'label' => 'form.label.headline'
        ]);

        $builder->add('body', TextareaType::class, [
            'attr' => [
                'class' => 'tinymce',
                'data-theme' => 'bbcode' // Skip it if you want to use default theme
            ],
            'required' => false,
            'label' => 'form.label.body'
        ]);

        $builder->add('category', EntityType::class, [
            'class' => Category::class,
            'multiple' => false,
            'required' => false,
            'attr' => [
                'data-placeHolder' => 'Category',
                'class' => 'question-category',
            ],
            'label' => 'form.label.category'
        ]);

        $builder->add('publishAt', DateType::class, [
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'required' => true,
            'attr' => [
                'class' => 'date-picker',
            ],
            'label' => 'form.label.publishAt'
        ]);

        $builder->add('expiresAt', DateType::class, [
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'required' => false,
            'attr' => [
                'class' => 'date-picker',
            ],
            'label' => 'form.label.ExpiresAt'
        ]);

        $builder->add('isActive', CheckboxType::class, [
            'required' => false,
            'label' => 'form.label.isActive'
        ]);

    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Question::class,
            'translation_domain' => 'form'
        ]);
    }

    public function getName() {
        return 'question_type';
    }

}
