<?php

namespace AppBundle\Form\Filter;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\CheckboxFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class  QuestionFilterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('headline', TextFilterType::class, [
			'apply_filter' => [
				$this, 'valueLike'
			],
			'attr' => [
				'placeholder' => 'form.label.headline'
			],
			'label' => false,
			'label_attr' => [
				'style' => 'hidden'
			]
		]);

        $builder->add('body', TextFilterType::class, [
            'apply_filter' => [
                $this, 'valueLike'
            ],
            'attr' => [
                'placeholder' => 'form.label.body'
            ],
            'label' => false,
            'label_attr' => [
                'style' => 'hidden'
            ]
        ]);

		$builder->add('searchInAll', TextFilterType::class, [
			'apply_filter' => function(QueryInterface $filterQuery, $field, $values){
				if($values['value'] === null){
					return null;
				}

				$value = $values['value'];
                $query = $filterQuery->getQueryBuilder();
                $query
                    ->orWhere("q.headline LIKE :value")
                    ->orWhere("q.body LIKE :value")
                    ->setParameter('value', "%{$value}%");
			},
			'attr' => [
				'data-placeholder' => 'form.label.search_in_all',
				'placeholder' => 'form.label.search_in_all'
			],
			'label' => false,
			'label_attr' => [
				'style' => 'hidden'
			]
		]);

        $builder->add('publishAt', DateTimeType::class, [
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'apply_filter' => [
                $this, 'valueGrEq'
            ],
            'required' => false,
            'attr' => [
                'placeholder' => 'form.label.publishAt',
                'class' => 'date-picker'
            ],
            'label' => false,
            'label_attr' => [
                'style' => 'hidden'
            ]
        ]);

        $builder->add('expiresAt', DateTimeType::class, [
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'apply_filter' => [
                $this, 'valueLeEq'
            ],
            'required' => false,
            'attr' => [
                'placeholder' => 'form.label.expiresAt',
                'class' => 'date-picker'
            ],
            'label' => false,
            'label_attr' => [
                'style' => 'hidden'
            ]
        ]);

        $builder->add('isActive', CheckboxFilterType::class, [
            'apply_filter' => function(QueryInterface $filterQuery, $field, $values){
                $enabled = $values['value'] ? 0 : 1;

                $query = $filterQuery->getQueryBuilder();
                $query->andWhere($query->expr()->eq('q.isActive', $enabled));
            },
            'label' => 'form.label.not_active'
        ]);

        $builder->add('filter', SubmitType::class, [
            'label' => 'form.label.filter'
        ]);

		$builder->add('clear', SubmitType::class, [
		    'label' => 'form.label.clear',
            'attr' => [
                'class' => 'btn-danger'
            ]
        ]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			'csrf_protection'   => false,
			'validation_groups' => ['filtering'],
            'translation_domain' => 'form'
		]);
	}

	public function getName() {
		return 'user_filter_type';
	}

	public function valueReverseEq(QueryInterface $filterQuery, $field, $values) {
		$paramName = sprintf('p_%s', str_replace('.', '_', $field));

		$expression = $filterQuery->getExpr()->eq($field, ':'.$paramName);
		$parameters = [
			$paramName => !$values['value']
		];

		return $filterQuery->createCondition($expression, $parameters);
	}

	public function valueEq(QueryInterface $filterQuery, $field, $values) {
		if (empty($values['value'])) {
			return null;
		}

		$paramName = sprintf('p_%s', str_replace('.', '_', $field));

		$expression = $filterQuery->getExpr()->eq($field, ':'.$paramName);
		$parameters = [
			$paramName => $values['value']
		];

		return $filterQuery->createCondition($expression, $parameters);
	}

	public function valueLike(QueryInterface $filterQuery, $field, $values) {
		if (empty($values['value'])) {
			return null;
		}

		$paramName = sprintf('p_%s', str_replace('.', '_', $field));
		$expression = $filterQuery->getExpr()->like($field, ':' . $paramName);
		$parameters = array($paramName => "%{$values['value']}%");

		return $filterQuery->createCondition($expression, $parameters);
	}

}
