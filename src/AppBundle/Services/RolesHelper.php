<?php

namespace AppBundle\Services;

class RolesHelper {

    private $rolesHierarchy;

    public function __construct(
        $rolesHierarchy
    ) {
        $this->rolesHierarchy = $rolesHierarchy;
    }

    public function getRoles() {
        $roles = array();
        foreach ($this->rolesHierarchy as $key => $value) {
            $roles[] = $key;

            foreach ($value as $value2) {
                $roles[] = $value2;
            }
        }
        return array_unique($roles);
    }

    public function flattenAssocArray() {
        $returnData = [];

        foreach($this->getRoles() as $key) {
            $tempValue = str_replace("ROLE_", '', $key);
            $tempValue = ucwords(strtolower(str_replace("_", ' ', $tempValue)));
            $returnData[$key] = $tempValue;
        }

        return $returnData;
    }

}
