<?php

namespace AppBundle\Utils;

use AppBundle\Entity\IEntity;
use ArrayObject;

class ArrayList extends ArrayObject {

    public function __construct($array = array()){
        parent::__construct($array, ArrayObject::ARRAY_AS_PROPS);
    }

    public function toArray() {
        return parent::getArrayCopy();
    }

    public function __ToString() {
        return 'Array';
    }

    public function getArrayCopy() {
        return array_map(function(IEntity $entity) {
            return $entity->getArrayCopy();
        }, parent::getArrayCopy());
    }

}
