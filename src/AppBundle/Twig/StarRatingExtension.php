<?php

namespace AppBundle\Twig;

use AppBundle\Entity\ContentRate\ContentRateFacade;
use Symfony\Component\HttpFoundation\RequestStack;

class StarRatingExtension extends \Twig_Extension
{

    private $requestStack;
    private $contentRateFacade;

    public function __construct(
        RequestStack $requestStack,
        ContentRateFacade $contentRateFacade
    ) {
        $this->requestStack = $requestStack;
        $this->contentRateFacade = $contentRateFacade;
    }

    public function getFunctions() {
        return array(
            'starBar' => new \Twig_Function_Method($this, 'getStarBar', ['needs_environment' => true]),
        );
    }

    public function getStarBar(\Twig_Environment $environment, $contentId, $rate, $rank, $starNum, $starWidth, $status) {
        $view = $this->contentRateFacade->prepareView($contentId, $rate, $rank, $starNum, $starWidth, $status);
        return $environment->render($view['view'], $view['params']);
    }

    public function getName() {
        return 'star_rating_extension';
    }

}
