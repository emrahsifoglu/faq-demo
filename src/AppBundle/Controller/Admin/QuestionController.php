<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category\Category;
use AppBundle\Entity\Question\Question;
use AppBundle\Form\CategoryType;
use AppBundle\Form\Filter\QuestionFilterType;
use AppBundle\Form\QuestionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/question")
 * @Security("has_role('ROLE_ADMIN')")
 */
class QuestionController extends Controller
{

    /**
     * @Route("/", name="admin_question_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function indexAction(Request $request) {
        $filter = $this->createForm(QuestionFilterType::class);
        $filter->handleRequest($request);

        if ($filter->isSubmitted() && $filter->isValid() && $filter->get('filter')->isClicked()) {
            $this->get('session')->set('question-filter', $filter->getData());
            return $this->redirectToRoute('admin_question_index');
        } elseif ($filter->get('clear')->isClicked()) {
            $this->get('session')->set('question-filter', []);
            return $this->redirectToRoute('admin_question_index');
        }

        $Q = $this->get('app.facade_question')->getRepository()->createQueryBuilder('q');

        if (!$filter->isSubmitted()) {
            $filter->setData($this->get('session')->get('question-filter', []));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $Q);
        }

        $questions = $Q->getQuery()->getResult();

        return [
            'filter' => $filter->createView(),
            'questions' => $questions,
            'index' => false
        ];
    }

    /**
     * @Route("/{id}/edit", name="admin_question_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param Question $question
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Question $question) {
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_question')->save($question);
            $this->addFlash('success', 'question has been updated');
            return $this->redirectToRoute('admin_question_index');
        }

        return [
            'question' => $question,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/new", name="admin_question_new")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function newAction(Request $request) {
        $question = new Question();

        if ($id = $this->get('session')->get('category/id')) {
            $this->get('session')->set('category/id', null);
            $category = $this->get('app.facade_category')->getRepository()->find($id);
            $question->setCategory($category);
        }

        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_question')->save($question);
            $this->addFlash('success', 'question has been created');
            return $this->redirectToRoute('admin_question_index');
        }

        return [
            'question' => $question,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/{id}/delete", name="admin_question_delete")
     * @Method("GET")
     * @param Question $question
     * @return RedirectResponse
     */
    public function deleteAction(Question $question = null) {
        if (!$question) {
            $this->addFlash('alert', 'Question might have been deleted');
            return $this->redirectToRoute('admin_question_index');
        }

        $this->get('app.facade_question')->delete($question);
        $this->addFlash('success', 'Question has been deleted');
        return $this->redirectToRoute('admin_question_index');
    }

    /**
     * @Route("/{id}/enable/", name="admin_question_enable")
     * @Method("GET")
     * @param Question $question
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function enabledAction(Question $question) {
        $question->setIsActive(true);
        $this->get('app.facade_question')->save($question);
        $this->addFlash('success', 'Question has been enabled');
        return $this->redirectToRoute('admin_question_index');
    }

    /**
     * @Route("/{id}/disable/", name="admin_question_disable")
     * @Method("GET")
     * @param Question $question
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function disableAction(Question $question) {
        $question->setIsActive(false);
        $this->get('app.facade_question')->save($question);
        $this->addFlash('success', 'Question has been disabled');
        return $this->redirectToRoute('admin_question_index');
    }

}
