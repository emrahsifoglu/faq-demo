<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category\Category;
use AppBundle\Form\CategoryType;
use AppBundle\Form\Filter\CategoryFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/category")
 * @Security("has_role('ROLE_ADMIN')")
 */
class CategoryController extends Controller
{

    /**
     * @Route("/", name="admin_category_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function indexAction(Request $request) {
        $filter = $this->createForm(CategoryFilterType::class);
        $filter->handleRequest($request);

        if ($filter->isSubmitted() && $filter->isValid() && $filter->get('filter')->isClicked()) {
            $this->get('session')->set('category-filter', $filter->getData());
            return $this->redirectToRoute('admin_category_index');
        } elseif ($filter->get('clear')->isClicked()) {
            $this->get('session')->set('category-filter', []);
            return $this->redirectToRoute('admin_category_index');
        }

        $Q = $this->get('app.facade_category')->getRepository()->createQueryBuilder('c');

        if (!$filter->isSubmitted()) {
            $filter->setData($this->get('session')->get('category-filter', []));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $Q);
        }

        $categories = $Q->getQuery()->getResult();

        return [
            'filter' => $filter->createView(),
            'categories' => $categories,
            'index' => false
        ];
    }

    /**
     * @Route("/{id}/edit", name="admin_category_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param Category $category
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Category $category) {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_category')->save($category);
            $this->addFlash('success', 'category has been updated');
            return $this->redirectToRoute('admin_category_index');
        }

        return [
            'category' => $category,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/new", name="admin_category_new")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function newAction(Request $request) {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_category')->save($category);
            $this->addFlash('success', 'category has been created');
            return $this->redirectToRoute('admin_category_index');
        }

        return [
            'category' => $category,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/{id}/delete", name="admin_category_delete")
     * @Method("GET")
     * @param Category $category
     * @return RedirectResponse
     */
    public function deleteAction(Category $category = null) {
        if (!$category) {
            $this->addFlash('alert', 'Category might have been deleted');
            return $this->redirectToRoute('admin_category_index');
        }


        if($category->hasQuestions()) {
            $this->addFlash('danger', 'Category has question(s)');
            return $this->redirectToRoute('admin_category_index');
        }

        $this->get('app.facade_category')->delete($category);
        $this->addFlash('success', 'Category has been deleted');
        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * @Route("/load", name="admin_category_load")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function loadAllClosedAction(Request $request) {
        $totalCount = $this->get('app.facade_category')->getRepository()->findTotalActiveCount();
        $name = $request->get('name');
        $categories = $this->get('app.facade_category')->getRepository()->findAllByPartialQuery($name);

        $items = [];

        /** @var Category $category */
        foreach ($categories as $category) {
            $items[] = [
                'id' => $category->getId(),
                'name' => $category->getHeadline()
            ];
        }

        return new JsonResponse([
            'items' => $items,
            'totalCount' => $totalCount
        ]);
    }

    /**
     * @Route("/{id}/enable/", name="admin_category_enable")
     * @Method("GET")
     * @param Category $category
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function enabledAction(Category $category) {
        $category->setIsActive(true);
        $this->get('app.facade_category')->save($category);
        $this->addFlash('success', 'Category has been enabled');
        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * @Route("/{id}/disable/", name="admin_category_disable")
     * @Method("GET")
     * @param Category $category
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function disableAction(Category $category) {
        $category->setIsActive(false);
        $this->get('app.facade_category')->save($category);
        $this->addFlash('success', 'Category has been disabled');
        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * @Route("/{id}/question/add", name="admin_category_question_add")
     * @Method("GET")
     * @param Category $category
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function questionAddAction(Category $category) {
         $this->get('session')->set('category/id', $category->getId());
         return $this->redirectToRoute('admin_question_new');
    }

}
