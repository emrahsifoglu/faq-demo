<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User\User;
use AppBundle\Form\Filter\UserFilterType;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends controller
{

    /**
     * @Route("/", name="admin_user_index")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function indexAction(Request $request) {
        $filter = $this->createForm(UserFilterType::class);
        $filter->handleRequest($request);

        if ($filter->isSubmitted() && $filter->isValid() && $filter->get('filter')->isClicked()) {
            $this->get('session')->set('user-filter', $filter->getData());
            return $this->redirectToRoute('admin_user_index');
        } elseif ($filter->get('clear')->isClicked()) {
            $this->get('session')->set('user-filter', []);
            return $this->redirectToRoute('admin_user_index');
        }

        $Q = $this->get('app.facade_user')->getRepository()->createQueryBuilder('u');

        if (!$filter->isSubmitted()) {
            $filter->setData($this->get('session')->get('user-filter', []));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $Q);
        }

        $users = $Q->getQuery()->getResult();

        return [
            'filter' => $filter->createView(),
            'users' => $users,
            'index' => false
        ];
    }

    /**
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|array
     */
    public function editAction(Request $request, User $user) {
        $form = $this->createForm(UserType::class, $user, [
            'intention' => 'edit',
            'editor' => 'admin'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_user')->updateUser($user);
            $this->addFlash('success', 'User has been edited');
            return $this->redirectToRoute('admin_user_index');
        }

        return [
            'user' => $user,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/new", name="admin_user_new")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @return RedirectResponse|array
     */
    public function newAction(Request $request) {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'intention' => 'create',
            'editor' => 'admin'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_user')->updateUser($user);
            $this->addFlash('success', 'User has been created');
            return $this->redirectToRoute('admin_user_index');
        }

        return [
            'user' => $user,
            'form' => $form->createView(),
            'create' => false
        ];
    }

    /**
     * @Route("/{id}/delete", name="admin_user_delete")
     * @Method("GET")
     * @param User $user
     * @return RedirectResponse
     */
    public function deleteAction(User $user) {
        if ($this->getUser() == $user) {
            throw new NotFoundHttpException();
        }

        $this->get('app.facade_user')->delete($user);
        $this->addFlash('success', 'User has been deleted');
        return $this->redirectToRoute('admin_user_index');
    }

}