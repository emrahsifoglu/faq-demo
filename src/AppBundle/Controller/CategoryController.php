<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/category")
 */
class CategoryController extends Controller
{

    /**
     * @Route("/", name="front_category_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $categories = $this->get('app.facade_category')->getActiveCategories();
        return [
            'categories' => $categories,
            'section' => 'category'
        ];
    }

    /**
     * @Route("/{slug}", name="front_category_show")
     * @ParamConverter("category",  class="AppBundle:Category\Category", options={
     *    "repository_method" = "findOneActiveBySlug",
     *    "mapping": { "slug": "slug" },
     *    "map_method_signature" = true
     * })
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @param Category $category
     * @return array|Response
     */
    public function showAction(Request $request, Category $category = null) {
        if (!$category) {
            $this->addFlash('danger', 'Category is not found');
            return $this->redirectToRoute('front_category_index');
        }

        return [
            'category' => $category
        ];

    }

    /**
     * @Route("/{slug}/question", name="front_category_question_show")
     * @ParamConverter("category",  class="AppBundle:Category\Category", options={
     *    "repository_method" = "findOneActiveBySlug",
     *    "mapping": { "slug": "slug" },
     *    "map_method_signature" = true
     * })
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @param Category $category
     * @return array|Response
     */
    public function showQuestionAction(Request $request, Category $category = null) {
        if (!$category) {
            $this->addFlash('danger', 'Category is not found');
            return $this->redirectToRoute('front_category_index');
        }

        return [
            'category' => $category
        ];

    }

}
