<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Question\Question;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/question")
 */
class QuestionController extends Controller
{

    /**
     * @Route("/", name="front_question_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $questions = $this->get('app.facade_question')->getPublicQuestions();
        return [
            'questions' => $questions
        ];
    }

    /**
     * @Route("/{slug}", name="front_question_show")
     * @ParamConverter("question",  class="AppBundle:Question\Question", options={
     *    "repository_method" = "findOnePublicBySlug",
     *    "mapping": { "slug": "slug" },
     *    "map_method_signature" = true
     * })
     * @Method("GET")
     * @Template()
     * @param Request $request
     * @param Question $question
     * @return array|Response
     */
    public function showAction(Request $request, Question $question = null) {
        if (!$question) {
            $this->addFlash('danger', 'Question is not found');
            return $this->redirectToRoute('front_question_index');
        }

        return [
            'question' => $question
        ];

    }

}
