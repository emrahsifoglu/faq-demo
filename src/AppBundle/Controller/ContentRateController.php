<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/content")
 * @Security("has_role('ROLE_USER')")
 */
class ContentRateController extends Controller
{

    /**
     * @Route("/rate", name="front_content_rate")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function contentRateAction(Request $request) {
        $contentId = $request->get('contentId');
        $rate = $request->get('rate');
        $starNum = $request->get('starNum');

        $question = $this->get('app.facade_question')->getRepository()->findOnePublicById($contentId);
        $questionRate = $this->get('app.facade_content_rate')->rateQuestion($this->getUser(), $question, $rate);

        $rank = $questionRate->getRank();
        $rate = $questionRate->getRate();

        $view = $this->get('app.facade_content_rate')->prepareView($contentId, $rate, $rank, $starNum, 25, 'voted');
        return new JsonResponse([
            'starGroup' => $this->renderView($view['view'], $view['params'])
        ]);
    }

}
