<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends Controller
{

    /**
     * @Route("/", name="front_user_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {

    }

    /**
     * @Route("/{id}/edit", name="front_user_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|array
     */
    public function editAction(Request $request, User $user) {
        $form = $this->createForm(UserType::class, $user, [
            'intention' => 'edit',
            'editor' => 'user'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.facade_user')->updateUser($user);
            $this->addFlash('success', 'User has been edited');
            return $this->redirectToRoute('front_user_edit', [
               'id' => $user->getId()
            ]);
        }

        return [
            'user' => $user,
            'form' => $form->createView(),
        ];
    }

}