<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/react")
 */
class ReactController extends Controller
{
    /**
     * @Route("/", name="front_react_index")
     * @Template()
     * @param Request $request
     */
    public function indexAction(Request $request) {

    }

    /**
     * @Route("/category/", name="front_react_category")
     * @param Request $request
     * @return JsonResponse
     */
    public function categoryAction(Request $request) {
        if(!$request->getContentType() == 'json') {
            return $this->forward('AppBundle:React:index');
        }
        $categories = $this->get('app.facade_category')->getCategorylist()->getArrayCopy();
        return new JsonResponse($categories);
    }

    /**
     * @Route("/question/", name="front_react_question")
     * @param Request $request
     * @return JsonResponse
     */
    public function questionAction(Request $request) {
        if(!$request->getContentType() == 'json') {
            return $this->forward('AppBundle:React:index');
        }
        $questions = $this->get('app.facade_question')->getQuestionList()->getArrayCopy();
        return new JsonResponse($questions);
    }

}
