<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User\User;
use AppBundle\Form\UserRegisterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{

	/**
	 * @Route("/register", name="register")
     * @Method({"GET", "POST"})
     * @Template()
	 * @param Request $request
	 * @return array|RedirectResponse
     */
	public function registerAction(Request $request) {
		// Create a new blank user and process the form
		$user = new User();
		$form = $this->createForm(UserRegisterType::class, $user);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->get('app.facade_user')->updateUser($user);

			return $this->redirectToRoute('login');
		}

		return [
		    'form' => $form->createView()
        ];
	}
}