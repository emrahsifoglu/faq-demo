<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class ContentType extends AbstractEnumType
{
    const CATEGORY = 'CATEGORY';
    const QUESTION = 'QUESTION';

    protected static $choices = [
        self::CATEGORY => 'Category',
        self::QUESTION => 'Question',
    ];

}
